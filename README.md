# HoloMonitor Optics Models

This repository contains SolidWorks CAD files for the Mark V Holographic Video Monitor. Included are several models for reference use, as well as optical mounts to be 3D printed and included in the monitor itself. Each full-color monitor needs the following parts from this repo:
* 1 `Beam Translator Holder`
* A `galvo mating device double` and `riser_base_drying_rack`, though they aren't consumed in construction
* 1 `Lens Holder`
* 1 complete `new_riser_mirror_mount`, which includes:
  * 1 `riser_mirror_base`
  * 1 `riser_mirror_top`
  * 1 `mirror_kinematic_holder_wafer`
* 1 `x_cube_holder`
* 1 `Polygon_Galvo Mount` (although some parts of it need to be suppressed in order to print just the housing)
* 1 `Parabaloidal Mirror Mount` assuming you have a way of attatching a mirror to it. Otherwise, just place half of a parabolic mirror on top of the monitor

# License

All files in this repository are Copyright 2015-2019 BYU ElectroHolography Research Group and are licensed under GNU General Public License v3 ([https://www.gnu.org/licenses/gpl-3.0.en.html](https://www.gnu.org/licenses/gpl-3.0.en.html))

![GPL v3 Logo](https://www.gnu.org/graphics/gplv3-127x51.png "GPL v3")
